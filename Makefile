build:
	docker-compose -f ./docker-compose.yml build --parallel --force-rm
run:
	docker-compose -f ./docker-compose.yml up --detach --no-recreate