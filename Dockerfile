# Dockerfile References: https://docs.docker.com/engine/reference/builder/

# Builder
FROM golang:1.13 as builder

RUN mkdir -p /opt/glide-example
WORKDIR /tmp/glide-example

COPY . .
RUN CGO_ENABLED=0 go build -o /opt/glide-example/app .

# Release
FROM alpine:latest
LABEL maintainer="Grigoriy Zavodov <zavodov@gmail.com>"

RUN mkdir -p /glide-example
WORKDIR /glide-example

COPY --from=builder /opt/glide-example ./

ENTRYPOINT ["./app"]
